const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
const mazeModel= []
const board = document.getElementById('mazeContainer') 

let walls="W";
let corridor= " ";
let playerLocation= "image";
let start= "S";
let finish="F";


function createMap() {
for (let rowIndex = 0; rowIndex < map.length; rowIndex++){
 
    let columnRow = document.createElement("div");
    columnRow.classList="classRow" //css .classRow
    

 for(let colIndex = 0; colIndex < map[rowIndex].length; colIndex++) {
        let cell = document.createElement("div")
cell.id= rowIndex +"-" + colIndex // to indetify *****


        if (map[rowIndex][colIndex]== 'W'){ 
            cell.classList= "wall" 

        }else if(map[rowIndex][colIndex]== ' '){
            cell.classList="floor"

        }else if(map[rowIndex][colIndex]== "S"){
            cell.classList ="start"
            playerLocation.rowIndex= "row"
            playerLocation.colIndex= "cell"

        }else if(map[rowIndex][colIndex]== "F"){
            cell.classList="finish"
        }
        columnRow.appendChild(cell);
    }
    board.appendChild(columnRow);
}
}
createMap();
//added by Elizabeth
let player= document.createElement("div")
player.classList = "player"
//board.appendChild(player)
const begin= document.getElementById("9-0");
begin.appendChild(player);


document.addEventListener('keydown', (event) => {
 //function (event.key) {
        const parentCell= player.parentElement;
        currentCellCoordinates= parentCell.id.split("-")
        console.log(currentCellCoordinates)

            let nextRowIndex=0;
            let nextColumnIndex=0;

    if(event.key === "ArrowUp") {
            nextRowIndex = Number(currentCellCoordinates[0])-1;
            nextColumnIndex = Number(currentCellCoordinates[1])+0;
            console.log(nextColumnIndex)
            console.log(nextRowIndex)
          
 }else if(event.key === "ArrowDown") {
    nextRowIndex= Number(currentCellCoordinates[0])+1;
    nextColumnIndex= Number(currentCellCoordinates[1])+0;
    
}else if(event.key === "ArrowLeft") {
    nextRowIndex= Number(currentCellCoordinates[0])+0;
    nextColumnIndex= Number(currentCellCoordinates[1])-1;
     
}else if(event.key === "ArrowRight")  {
    nextRowIndex= Number(currentCellCoordinates[0])+0;
    nextColumnIndex= Number(currentCellCoordinates[1])+1
}
newCoordinates= nextRowIndex + "-" + nextColumnIndex;
newCoordinates= document.getElementById(newCoordinates);
if(newCoordinates.classList.contains ("wall")){
    return;
}
newCoordinates.appendChild(player)
if(newCoordinates.classList.contains("finish")){
  document.getElementById("winner").style.display="block"
    //document.write("Finish")
}

newCoordinates.appendChild(player);
//document.getElementById(newCoordinates).appendChild(player);
})